﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace XMLParser
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument document = new XmlDocument();
            document.Load("Types.xml");

            StringBuilder builder = new StringBuilder();
            StreamWriter writer = new StreamWriter("Operations.txt", false);

            XmlNodeList rootNodes = document.DocumentElement.SelectNodes("/Nuclides/element");

            int counter = rootNodes.Count + 1;

            foreach (XmlNode parentNode in rootNodes)
            {
                var atNbmr = parentNode.Attributes["ID"].InnerText;

                foreach (XmlNode node in parentNode.ChildNodes)
                {
                    builder.Clear();
                    builder.AppendLine("context.DictNuclides.Add(");
                    builder.AppendLine("new DictNuclide()");
                    builder.AppendLine("{");
                    builder.AppendFormat(@"Id = {0},
                                       AtomMass = {1},
                                       PeriodFactor = {2},
                                       Izomer = {3},
                                       ElementId = {4},
                                       AtomNumber = {4},",
                                           node.Attributes["ID"].InnerText,
                                           node.Attributes["Mass"].InnerText,
                                           node.Attributes["DecayPeriodType"].InnerText,
                                           node.Attributes["Izomer"].InnerText,
                                           atNbmr);

                    builder.AppendLine("");

                    builder.AppendLine("DecayPeriod = " + Regex.Replace(node.Attributes["DecayPeriod"].InnerText, ",", ".") + ",");

                    if (node.Attributes["MinActivity"].InnerText == "")
                        builder.AppendLine("MinActivity = null,");
                    else
                        builder.AppendLine("MinActivity = " + Regex.Replace(node.Attributes["MinActivity"].InnerText, ",", ".") + ",");

                    if (node.Attributes["MinSpecActivity"].InnerText == "")
                        builder.AppendLine("MinSpecActivity = null,");
                    else
                        builder.AppendLine("MinSpecActivity = " + Regex.Replace(node.Attributes["MinSpecActivity"].InnerText, ",", ".") + ",");

                    if (node.Attributes["DCat"].InnerText == "")
                        builder.AppendLine("DCategory = null,");
                    else
                        builder.AppendLine("DCategory = " + Regex.Replace(node.Attributes["DCat"].InnerText, ",", ".") + "");

                    builder.AppendLine("");
                    builder.AppendLine("}");
                    builder.AppendLine(");");

                    writer.Write(builder.ToString());
                }

                

                counter--;

                Console.WriteLine("Осталось: {0} строк", counter);
            }

            writer.Close();
            Console.WriteLine("Парсинг завершен");

            Console.ReadLine();
        }
    }
}
